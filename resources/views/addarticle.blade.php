@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Add Articles</div>
                <div class="panel-body">
                    <form action="{{ url('/articles') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="articleTitle">Title</label>
                            <input name="title" type="text" class="form-control" id="articleTitle" placeholder="Title">
                            
                        </div>
                        <div class="form-group">
                            <label for="article">Article</label>
                            <textarea name="article" class="form-control" rows="3" placeholder="Write your article here.."></textarea>

                        </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
