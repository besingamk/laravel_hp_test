@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Articles</div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ url('/articles/create') }}" class="btn btn-primary">Add Article</a>
                        </div>
                    </div>
                    <div class="row">
                        
                        @foreach ($articles as $article)
                            <div class="col-md-12">
                                <h1>{{ $article->title }}</h1>
                                <p>by: {{ $article->user->name }}</p>
                                <p>{{ $article->article }}</p>
                                <div class="btn-group" role="group" aria-label="...">
                                  <a href="{{ URL::to('/articles/' . $article->id .'/edit') }}" class="btn btn-info">Edit</a>
                                  <a href="{{ URL::to('/articles/' .$article->id. '/delete') }}" class="btn btn-danger">Delete</a>
                                </div>
                                <hr />
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
