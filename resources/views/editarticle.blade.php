@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Articles</div>
                <div class="panel-body">
                    <form method="post" action="{{ URL::to('/articles/saveEdit/' . $article->id) }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="articleTitle">Title</label>
                            <input name="title" type="text" class="form-control" id="articleTitle" value="{{ $article->title }}" placeholder="Title">
                            
                        </div>
                        <div class="form-group">
                            <label for="article">Article</label>
                            <textarea name="article" class="form-control" rows="3" placeholder="Write your article here..">{{ $article->article }}</textarea>

                        </div>
                        <input type="submit" class="btn btn-primary" value="update">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
