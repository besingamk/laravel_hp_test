<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = "articles";

    protected $fillable = ['title', 'article'];

    protected $dates = [
        'deleted_at',
    ];

    public function user() 
    {
    	return $this->belongsTo('App\User');
    }
}
