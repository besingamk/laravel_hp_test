<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['middleware' => ['web']], function () {
	Route::auth();

	Route::get('/home', 'HomeController@index');

	Route::group(['middleware' => 'auth:web'], function () {
		Route::post('/articles/saveEdit/{id}', 'ArticleController@saveEdit');
		Route::get('/articles/{id}/delete', 'ArticleController@deleteArticle');
		Route::resource('/articles', 'ArticleController');
	});

});
