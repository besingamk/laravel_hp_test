<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Article;
use Gate;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::with('user')->get();
        return view('article')->with('articles', $articles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('addarticle');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'article' => 'required',
        ]);

         if ($validator->fails()) {
            return redirect('articles/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        $article = new Article;

        $article->article = $request->get('article');
        $article->title = $request->get('title');
        $article->user_id = Auth::guard('web')->user()->id;
        $article->save();

        return redirect('articles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);
        if (Gate::forUser(Auth::guard('web')->user())->allows('update-article', $article)) {
            //
            return view('editarticle')->with('article', $article);
        }

        return response(['message' => 'you cant do that'], 403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function saveEdit(Request $request, $id) 
    {
        $article = Article::find($id);

        $article->title = $request->get('title');
        $article->article = $request->get('article');

        $article->save();

        return redirect('/articles');
    }

    public function deleteArticle($id) {
        $article = Article::find($id);

        if (Gate::forUser(Auth::guard('web')->user())->allows('delete-article', $article)) {
            $article->delete();
            return redirect('/articles');
        }
        return response(['message' => 'you cant do that'], 403);
        
    }
}
